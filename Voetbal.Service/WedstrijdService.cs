﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Voetbal.Models;
using Voetbal.DAO;

namespace Voetbal.Service
{
    public class WedstrijdService
    {
        WedstrijdDAO wedstrijdDAO;

        public IEnumerable<Wedstrijd> All()
        {
            wedstrijdDAO = new WedstrijdDAO();
            return wedstrijdDAO.All();
        }

        public IEnumerable<Wedstrijd> WedstrijdBySpeeldag(int id)
        {
            wedstrijdDAO = new WedstrijdDAO();
            return wedstrijdDAO.WedstrijdBySpeeldag(id);
        }
    }
}
