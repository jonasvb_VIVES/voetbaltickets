﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Voetbal.Models;
using Voetbal.DAO;

namespace Voetbal.Service
{
    public class SpeeldagService
    {
        SpeeldagDAO speeldagDAO;
        public IEnumerable<Speeldag> All()
        {
            speeldagDAO = new SpeeldagDAO();
            return speeldagDAO.All();
        }
    }
}
