﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Voetbal.Models;
using System.Data.Entity;

namespace Voetbal.DAO
{
    public class WedstrijdDAO
    { 
        public IEnumerable<Wedstrijd> All()
        {
            using (var db = new VoetbalDBEntities())
            {
                return db.Wedstrijds.Include(t => t.Club).Include(b => b.Club1).Include(s => s.Speeldag).Include(t => t.Stadion).ToList();
            }
        }

        public IEnumerable<Wedstrijd> WedstrijdBySpeeldag(int id)
        {
            using (var db = new VoetbalDBEntities())
            {
                return db.Wedstrijds.Where(s => s.Speeldag.Id == id).Include(t => t.Club).Include(b => b.Club1).Include(s => s.Speeldag).Include(t => t.Stadion).ToList();
            }
        }
    }
}
