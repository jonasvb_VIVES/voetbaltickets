﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Voetbal.Models;
using System.Data.Entity;

namespace Voetbal.DAO
{
    public class SpeeldagDAO
    {
        public IEnumerable<Speeldag> All()
        {
            using (var db = new VoetbalDBEntities())
            {
                return db.Speeldags.ToList();
            }
        }
    }
}
