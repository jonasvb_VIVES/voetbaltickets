﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Voetbal.Models;
using Voetbal.Service;

namespace Voetbal.Controllers
{
    public class WedstrijdController : Controller
    {
        WedstrijdService wedstrijdService;
        SpeeldagService speeldagService;
        public ActionResult Index()
        {
            speeldagService = new SpeeldagService();
            ViewBag.listSpeeldagen = new SelectList(speeldagService.All(), "Id", "Datum");
            return View();
        }

        [HttpPost]
        public ActionResult Index(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            wedstrijdService = new WedstrijdService();
            var listWedstrijden = wedstrijdService.WedstrijdBySpeeldag(Convert.ToInt16(Id));

            speeldagService = new SpeeldagService();
            ViewBag.listSpeeldagen = new SelectList(speeldagService.All(), "Id", "Id", "Id");

            return View(listWedstrijden);
        }
    }
}